# -*- coding: utf-8 -*-
from __future__ import absolute_import
"""
Definition and configuration of the various web-front end cherrypy handlers.
"""

import cherrypy
import collections
import csv
import datetime
import decimal
import hydro
import itertools
import itertools
import jinja2
import math
import os
import pyrqlate
import requests
import StringIO
from geniusql import logic

import cream.tools
import cream.timeutils
from cream import rpc, rql, form
from plumbing import auth, email as plumbing_email
import gamestradamus
from gamestradamus import email, timeutils, units
from gamestradamus.form import to_decimal
from gamestradamus.taskqueue import tasks, celery
from gamestradamus.itertools import closest_two, closest_two_bisect
from gamestradamus.regions import REGIONS
from gamestradamus.thirdparty import crypto
from gamestradamus.handlers import admin

Converters = cream.timeutils.Converters

#-------------------------------------------------------------------------------
def color_hash(s):
    return sum([ord(x) ^ 2 for x in s]) % 8

#-------------------------------------------------------------------------------
class PredictionHandler(rpc.dejavu.UnitHandler):
    """An API handler for managing the making of predictions."""

    #---------------------------------------------------------------------------
    @cherrypy.expose(alias='last-game-library-update')
    @auth.handlers.Authenticated()
    @rpc.Signature(rpc.Type.Datetime, [])
    def last_game_library_update(self, **kwargs):
        user_account = cherrypy.serving.request.user_account

        user_attrs = user_account.UserAccountAttributes()

        private_profile_since = None
        if user_attrs.private_profile_since:
            private_profile_since = Converters.iso_from_datetime(
                user_attrs.private_profile_since
            )

        if user_attrs and user_attrs.games_last_updated:
            count = user_account.sandbox.count(
                units.UserAccountGame,
                lambda x: x.user_account_id == user_account.id
            )
            return {
                'last_updated': Converters.iso_from_datetime(
                    user_attrs.games_last_updated
                ),
                'private_profile_since': private_profile_since,
                'count': count
            }

        return {
            'private_profile_since': private_profile_since,
            'last_updated': None,
            'count': 0
        }

    #---------------------------------------------------------------------------
    @cherrypy.expose(alias='last-family-tree-update')
    @auth.handlers.Authenticated()
    @rpc.Signature(rpc.Type.Datetime, [])
    def last_family_tree_update(self, **kwargs):
        user_account = cherrypy.serving.request.user_account

        user_attrs = user_account.UserAccountAttributes()
        if user_attrs and user_attrs.relatives_last_updated:
            return Converters.iso_from_datetime(
                user_attrs.relatives_last_updated
            )
        return None

    #---------------------------------------------------------------------------
    @cherrypy.expose(alias='update-games')
    @auth.handlers.Authenticated()
    @rpc.Signature(rpc.Type.Boolean, [])
    def update_games(self, **kwargs):
        user_account = cherrypy.serving.request.user_account

        steam_id_64 = user_account.openid.rsplit('/', 1)[-1]

        user_attrs = user_account.UserAccountAttributes()
        if user_attrs:
            # Don't allow them to update their predictions more than once
            # per minute.
            one_minute_ago = timeutils.utcnow() - datetime.timedelta(minutes=1)
            if user_attrs.games_last_updated > one_minute_ago:
                return False

        routing_key = "tasks.plebs.update-games"
        if user_attrs and user_attrs.premium_account:
            routing_key = "tasks.premium.update-games"

        result = tasks.steam_update_hours_in_game.apply_async(**{
            'args': [steam_id_64],
            'routing_key': routing_key,
        })
        return True

    #---------------------------------------------------------------------------
    @cherrypy.expose(alias='update-predictions')
    @auth.handlers.Authenticated()
    @rpc.Signature(rpc.Type.Boolean, [])
    def update_predictions(self, **kwargs):
        user_account = cherrypy.serving.request.user_account

        user_attrs = user_account.UserAccountAttributes()
        if user_attrs:
            user_attrs.predictions_updating = True
            # Don't allow them to update their predictions more than once
            # an hour.
            one_hour_ago = timeutils.utcnow() - datetime.timedelta(hours=1)
            if user_attrs.predictions_last_updated > one_hour_ago:
                return False

        routing_key = "tasks.plebs.update-predictions"
        if user_attrs and user_attrs.premium_account:
            routing_key = "tasks.premium.update-predictions"

        steam_id_64 = user_account.openid.rsplit('/', 1)[-1]
        result = celery.chain(
            tasks.predictions_update_family_tree.s(None, steam_id_64, 200),
            tasks.predictions_prophesy.s(steam_id_64, 120)
        ).apply_async(routing_key=routing_key)
        return True

    #---------------------------------------------------------------------------
    @cherrypy.expose(alias='last-predictions-update')
    @auth.handlers.Authenticated()
    @rpc.Signature(rpc.Type.Datetime, [])
    def last_predictions_update(self, **kwargs):
        user_account = cherrypy.serving.request.user_account

        user_attrs = user_account.UserAccountAttributes()
        if user_attrs and user_attrs.predictions_last_updated:
            return Converters.iso_from_datetime(
                user_attrs.predictions_last_updated
            )
        return None

#-------------------------------------------------------------------------------
@cherrypy.popargs('user_account_id')
class MemberHandler(object):

    #---------------------------------------------------------------------------
    @cherrypy.expose()
    @auth.tools.SigninRequired()
    @cream.tools.JinjaTemplate('/gamestradamus/member/games.html')
    def games(self, user_account_id, **kwargs):
        """Provide a read only version of a members game list.
        """

        request = cherrypy.serving.request
        try:
            user_account_id = int(user_account_id)
            user_account = request.sandbox.UserAccount(id=user_account_id)
            if user_account is None:
                raise cherrypy.NotFound()

        except (ValueError, TypeError), e:
            raise cherrypy.NotFound()

        # Does the signed in profile have access to the members library?
        friend_list = [x[1] for x in request.sandbox.xrecall(
            units.UserAccountFriend << auth.units.UserAccount,
            lambda x, y: x.user_account_id == request.user_account.id
        )]
        if user_account_id not in [x.id for x in friend_list]:
            raise cherrypy.NotFound()

        # We should show the library in the correct region.
        user_attrs = user_account.UserAccountAttributes()
        user_region = REGIONS[user_attrs.region]

        price_attr = user_region['game_attributes']['price']
        sale_price_attr = user_region['game_attributes']['sale_price']
        on_sale_since_attr = user_region['game_attributes']['on_sale_since']

        def flatten(x):
            """Convert raw view data into named dicts."""
            d = [{
                'id': x[0],
                'ignore': x[1],
                'tags': x[2],
                'corrected': x[3],
                'dollars_paid': x[4],
                'custom_hours_played': x[5],
                'steam_hours_played': x[6],
                'cost_per_hour': x[7],
                'rating': x[8],
                'finished': x[9],

                # Pre-compute the hours played value.
                'hours_played': (x[5] or 0) + (x[6] or 0)
            }, {
                'id': x[10],
                'canonical_game_id': x[11],
                'name': x[12],
                'logo': x[13],
                'price': x[14],
                'sale_price': x[15],
                'on_sale': True if x[16] is not None else False,
                'genre': x[17],
                'app_id': x[18]
            }]

            if x[11]:
                # The game is an alias for another canonical game. Pull up the
                # pricing data for the canonical game.
                data = request.sandbox.store.view((
                    units.Game,
                    lambda g: (
                        getattr(g, price_attr),
                        getattr(g, sale_price_attr),
                        getattr(g, on_sale_since_attr)
                    ),
                    lambda g: g.id == x[11]
                ))

                d[1]['price'] = data[0][0]
                d[1]['sale_price'] = data[0][1]
                d[1]['on_sale'] = True if data[0][2] is not None else False

            return d

        # Show some data's
        data = request.sandbox.store.view((
            units.UserAccountGame << units.Game,
            lambda uag, g: (
                uag.id,
                uag.ignore,
                uag.tags,
                uag.corrected,
                uag.dollars_paid,
                uag.custom_hours_played,
                uag.steam_hours_played,
                uag.cost_per_hour,
                uag.rating,
                uag.finished,

                g.id,
                g.canonical_game_id,
                g.name,
                g.logo,
                getattr(g, price_attr),
                getattr(g, sale_price_attr),
                getattr(g, on_sale_since_attr),
                g.genre,
                g.app_id
            ),
            lambda x, y: x.user_account_id == user_account.id \
                and (x.ignore == False or x.ignore is None)
        ))
        results = [flatten(x) for x in data]
        results.sort(**{
            'key': lambda x: x[0]['hours_played'],
            'reverse': True
        })

        # Data required to display and filter by custom tags.
        user_account_tag_list = user_account.UserAccountTag()
        tag_group_lookup = dict([(x.name, x.group or None) for x in user_account_tag_list])
        group_tag_list = [(k, sorted(g, key=lambda x: x.name.lower())) for k, g in itertools.groupby(
            sorted(user_account_tag_list, key=lambda x: x.group or None),
            lambda x: x.group or None
        )]
        group_tag_list.sort(key=lambda x: x[0] if x[0] is not None else 'zzzzzzz')

        # Figure out which genres to show the user for filtering.
        genre_filter_list = request.sandbox.recall(
            units.Genre,
            lambda x: x.id < 14
        )

        rating_filter_list = [
            {'label': u'★★★★★', 'filter': '5'},
            {'label': u'★★★★', 'filter': '4'},
            {'label': u'★★★', 'filter': '3'},
            {'label': u'★★', 'filter': '2'},
            {'label': u'★', 'filter': '1'},
            {'label': u'Unrated', 'filter': '0'},
        ]
        finished_filter_list = [
            u'Yes',
            u'No',
        ]
        cost_filter_list = user_region['cost_filter_list']
        hours_filter_list = [
            {'label': '< 1 hr', 'filter': '0-1'},
            {'label': '1 - 8 hrs', 'filter': '1-8'},
            {'label': '8 - 20 hrs', 'filter': '8-20'},
            {'label': '20 - 40 hrs', 'filter': '20-40'},
            {'label': '40+ hrs', 'filter': '40-100000'}
        ]

        context = {
            'user_account': user_account,
            'region': user_region,
            'results': results,
            'color_hash': color_hash,
            'tag_group_lookup': tag_group_lookup,
            'group_tag_list': group_tag_list,
            'rating_filter_list': rating_filter_list,
            'finished_filter_list': finished_filter_list,
            'genre_filter_list': genre_filter_list,
            'cost_filter_list': cost_filter_list,
            'hours_filter_list': hours_filter_list,
            'friend_list': friend_list,

            'preserve_whitespace': True,
        }

        # This page should not be cached by browsers as it is extremely dynamic.
        cherrypy.response.headers['Cache-Control'] = 'no-cache'

        return context

    #---------------------------------------------------------------------------
    @cherrypy.expose()
    @auth.tools.SigninRequired()
    @cream.tools.JinjaTemplate('/gamestradamus/member/insights.html')
    def insights(self, user_account_id, **kwargs):
        """Provide a read only version of a members insight page.
        """

        request = cherrypy.serving.request
        try:
            user_account_id = int(user_account_id)
            user_account = request.sandbox.UserAccount(id=user_account_id)
            if user_account is None:
                raise cherrypy.NotFound()

        except (ValueError, TypeError), e:
            raise cherrypy.NotFound()

        # Does the signed in profile have access to the members library?
        friend_list = [x[1] for x in request.sandbox.xrecall(
            units.UserAccountFriend << auth.units.UserAccount,
            lambda x, y: x.user_account_id == request.user_account.id
        )]
        if user_account_id not in [x.id for x in friend_list]:
            raise cherrypy.NotFound()

        # Dump the data for the analysis
        def flatten(game):
            tags = collections.defaultdict(list)
            if game[8]: # if game.tags:
                for tag in game[8].split('|'):
                    pieces = tag.split(' / ')
                    if len(pieces) > 1:
                        tags['Tag/%s' % pieces[0]].append({
                            'label': pieces[1],
                            'filter': tag,
                        })

            genres = []
            if game[2]: # if game.genre
                for g in game[2].split(', '):
                    genres.append({
                        'label': g,
                        'filter': g
                    })

            return {
                'id': game[0],
                'n': game[1],
                'g': genres,
                'hp': float('%.1f' % ((game[3] or 0) + (game[4] or 0))),    # hours-played
                'dp': float('%.2f' % (game[5] or 0)),                       # dollars-paid
                'f': game[6] or False,                                      # finished
                'r': game[7] or 0,                                          # rating
                't': tags,                                                  # tags
            }
        data = request.sandbox.store.view((
            units.UserAccountGame << units.Game,
            lambda x, y: (
                y.id,
                y.name,
                y.genre,
                x.custom_hours_played,
                x.steam_hours_played,
                x.dollars_paid,
                x.finished,
                x.rating,
                x.tags
            ),
            lambda x, y: x.user_account_id == user_account.id \
                and (x.ignore == False or x.ignore is None)
        ))
        data = [flatten(x) for x in data]

        tags = user_account.UserAccountTag()
        tags = sorted(tags, key=lambda x: x.group or 'zzzzzz')
        tag_groups = []
        for k, g in itertools.groupby(tags, key=lambda x: x.group or None):
            tag_groups.append({'name': k, 'tags': list(g)})
        user_attrs = user_account.UserAccountAttributes()
        user_region = REGIONS[user_attrs.region]

        # This page should not be cached by browsers as it is extremely dynamic.
        cherrypy.response.headers['Cache-Control'] = 'no-cache'

        return {
            'user_account': user_account,
            'tag_groups': tag_groups,
            'data': data,
            'user_attrs': user_attrs,
            'region': user_region,
            'friend_list': friend_list,
        }

#-------------------------------------------------------------------------------
class ProfileHandler(object):

    _cp_config = {'tools.welcome.on': True}

    #---------------------------------------------------------------------------
    @cherrypy.expose()
    @auth.tools.SigninRequired()
    def index(self, *args, **kwargs):
        raise cherrypy.HTTPRedirect('/profile/games/')

    #---------------------------------------------------------------------------
    @cherrypy.expose()
    @auth.tools.SigninRequired()
    @cream.tools.JinjaTemplate('/gamestradamus/profile/welcome.html')
    def welcome(self, **kwargs):
        request = cherrypy.serving.request
        user_attrs = request.user_account.UserAccountAttributes()
        return {
            'welcome_started': Converters.iso_from_datetime(
                user_attrs.welcome_started or timeutils.utcnow()
            )
        }
    welcome._cp_config = {'tools.welcome.on': False}

    #---------------------------------------------------------------------------
    @cherrypy.expose()
    @auth.tools.SigninRequired()
    @cream.tools.JinjaTemplate('/gamestradamus/profile/browse.html')
    def browse(self, *args, **kwargs):
        request = cherrypy.serving.request

        price_attr = request.user_region['game_attributes']['price']
        sale_price_attr = request.user_region['game_attributes']['sale_price']
        on_sale_since_attr = request.user_region['game_attributes']['on_sale_since']
        available_since_attr = request.user_region['game_attributes']['available_since']

        order = {
            'cheapest': 'cheapest',
            'gauge': 'gauge',
            'hours': 'hours',
        }.get(kwargs.get('order', 'gauge'), 'gauge')
        genre = {
            'action': 'Action',
            'adventure': 'Adventure',
            'strategy': 'Strategy',
            'rpg': 'RPG',
            'indie': 'Indie',
            'massively multiplayer': 'Massively Multiplayer',
            'platformer': 'Platformer',
            'casual': 'Casual',
            'simulation': 'Simulation',
            'racing': 'Racing',
            'sports': 'Sports',
            'early access': 'Early Access',
        }.get(kwargs.get('genre', None), None)
        if genre:
            genre = request.sandbox.Genre(name=genre)
        price = to_decimal(kwargs.get('price', None), request.user_region)

        expr = logic.Expression(
            lambda x: x.is_dlc == False and getattr(x, available_since_attr) != None
        )
        join = units.Game
        if genre:
            expr = expr & logic.Expression(lambda x, y: y.genre_id == genre.id)
            join = join << units.GameGenre
        if price:
            expr = expr & logic.Expression(lambda x: getattr(x, sale_price_attr) <= price)

        games = request.sandbox.store.view((
            join,
            lambda x: (x.id, getattr(x, price_attr), getattr(x, sale_price_attr), getattr(x, on_sale_since_attr), x.median_hours_played, x.owner_count),
            expr
        ))
        x_id, x_price, x_sale_price, x_on_sale_since, x_median_hours, x_owner_count = range(6)

        # Any game that is already owned by the profile should be removed from the list.
        game_id_list = [g[x_id] for g in games]
        results = request.sandbox.store.view((
            units.UserAccountGame << units.Game,
            lambda x, y: (y.id, y.canonical_game_id),
            lambda x, y: x.user_account_id == request.user_account.id \
                and x.game_id in game_id_list
        ))

        owned_map = dict([(x[1] or x[0], True) for x in results])
        games = [g for g in games if not owned_map.get(g[x_id])]

        # Reorder the results.
        if order == 'gauge':
            games = [g for g in games if g[x_price]]

            def key(x):
                min_price = min(x[x_price] or 0, x[x_sale_price] or 0)
                if x[x_median_hours] and ((x[x_owner_count] or 0) > 19):
                    return (min(
                        min_price / x[x_median_hours],
                        min_price
                    ), x[x_id])
                else:
                    return (min_price, x[x_id])

            games.sort(**{'key': key})

        elif order == 'hours':
            games.sort(**{
                'key': lambda x: (x[x_median_hours] or 0, x[x_id]),
                'reverse': True
            })
        else:
            games = [g for g in games if g[x_price]]
            games.sort(**{
                'key': lambda x: (x[x_sale_price], x[x_id])
            })

        # Show only the games that are on the current page.
        total_count = len(games)
        page_count = int(math.ceil(len(games) / 24.0));
        try:
            page = int(kwargs.get('page', 1))
            if page < 1:
                page = 1
            elif page > page_count:
                page = page_count
        except (ValueError, TypeError), e:
            page = 1
        games = games[((page - 1) * 24):(page * 24)]

        # Figure out which pages to show to the user.
        lower = max(1, min(page_count - 4, page - 2))
        pages = [(lower + x) for x in range(5) if (lower + x) <= page_count]

        # Fetch units for the included game ids.
        game_id_list = [g[0] for g in games]
        games = request.sandbox.recall(
             units.Game,
             lambda x: x.id in game_id_list
        )

        # Sort the page results to ensure that they are consistent with the global sort.
        if order == 'gauge':
            games.sort(**{
                'key': lambda x: x.community_value,
            })
        elif order == 'hours':
            games.sort(**{
                'key': lambda x: (x.median_hours_played or 0, x.id),
                'reverse': True
            })
        else:
            games.sort(**{
                'key': lambda x: x.sale_price,
            })

        # Split the list into two lists for display
        games_one = []
        games_two = []

        for x in range(0, len(games)):
            if x % 2:
                games_two.append(games[x])
            else:
                games_one.append(games[x])

        return {
            'games_one': games_one,
            'games_two': games_two,
            'order': order,
            'page': page,
            'pages': pages,
            'page_count': page_count,
            'total_count': total_count,
            'genre': genre.name.lower() if genre else None,
            'price': price,
        }

    #---------------------------------------------------------------------------
    @cherrypy.expose()
    @auth.tools.SigninRequired()
    @cream.tools.JinjaTemplate('/gamestradamus/profile/sales.html')
    def sales(self, *args, **kwargs):
        request = cherrypy.serving.request

        price_attr = request.user_region['game_attributes']['price']
        sale_price_attr = request.user_region['game_attributes']['sale_price']
        on_sale_since_attr = request.user_region['game_attributes']['on_sale_since']
        available_since_attr = request.user_region['game_attributes']['available_since']

        order = {
                'recent': 'recent',
                'savings': 'savings',
                'cheapest': 'cheapest',
                'gauge': 'gauge',
                'hours': 'hours',
            }.get(kwargs.get('order', 'recent'), 'recent')
        genre = {
                'action': 'Action',
                'adventure': 'Adventure',
                'strategy': 'Strategy',
                'rpg': 'RPG',
                'indie': 'Indie',
                'massively multiplayer': 'Massively Multiplayer',
                'platformer': 'Platformer',
                'casual': 'Casual',
                'simulation': 'Simulation',
                'racing': 'Racing',
                'sports': 'Sports',
                'early access': 'Early Access',
            }.get(kwargs.get('genre', None), None)
        if genre:
            genre = request.sandbox.Genre(name=genre)
        price = to_decimal(kwargs.get('price', None), request.user_region)

        expr = logic.Expression(
            lambda x: getattr(x, on_sale_since_attr) is not None and x.is_dlc == False and getattr(x, available_since_attr) != None
        )
        join = units.Game
        if genre:
            expr = expr & logic.Expression(lambda x, y: y.genre_id == genre.id)
            join = join << units.GameGenre
        if price:
            expr = expr & logic.Expression(lambda x: getattr(x, sale_price_attr) <= price)

        games = request.sandbox.store.view((
            join,
            lambda x: (x.id, getattr(x, price_attr), getattr(x, sale_price_attr), getattr(x, on_sale_since_attr), x.median_hours_played, x.owner_count),
            expr
        ))
        x_id, x_price, x_sale_price, x_on_sale_since, x_median_hours, x_owner_count = range(6)

        # Any game that is already owned by the profile should be removed from the list.
        game_id_list = [g[x_id] for g in games]
        results = request.sandbox.store.view((
            units.UserAccountGame << units.Game,
            lambda x, y: (y.id, y.canonical_game_id),
            lambda x, y: x.user_account_id == request.user_account.id \
                and x.game_id in game_id_list
        ))

        owned_map = dict([(x[1] or x[0], True) for x in results])
        games = [g for g in games if not owned_map.get(g[x_id])]

        # Compile a list of unique genres in the result set.
        expr = logic.Expression(
            lambda x, y, z: getattr(x, on_sale_since_attr) is not None and x.is_dlc == False and getattr(x, available_since_attr) != None
        )
        if price:
            expr = expr & logic.Expression(
                lambda x, y, z: getattr(x, sale_price_attr) <= price
            )
        genres = request.sandbox.store.view((
                units.Game << units.GameGenre << units.Genre,
                lambda x, y, z: [x.id, z.name],
                expr
            ))
        genres = set([x[1].lower() for x in genres if x[1] and not owned_map.get(x[0])])

        # Reorder the results.
        if order == 'recent':
            games.sort(**{
                'key': lambda x: x[x_on_sale_since],
                'reverse': True,
            })
        elif order == 'savings':
            games.sort(**{
                'key': lambda x: ((x[x_price] - x[x_sale_price]) / x[x_price]),
                'reverse': True
            })
        elif order == 'gauge':

            def key(x):
                min_price = min(x[x_price] or 0, x[x_sale_price] or 0)
                if x[x_median_hours] and ((x[x_owner_count] or 0) > 19):
                    return min(
                        min_price / x[x_median_hours],
                        min_price
                    )
                else:
                    return min_price

            games.sort(**{'key': key})

        elif order == 'hours':
            games.sort(**{
                'key': lambda x: (x[x_median_hours] or 0, x[x_id]),
                'reverse': True
            })
        else:
            games.sort(**{
                'key': lambda x: x[x_sale_price],
            })

        # Show only the games that are on the current page.
        total_count = len(games)
        page_count = int(math.ceil(len(games) / 24.0));
        try:
            page = int(kwargs.get('page', 1))
            if page < 1:
                page = 1
            elif page > page_count:
                page = page_count
        except (ValueError, TypeError), e:
            page = 1
        games = games[((page - 1) * 24):(page * 24)]

        # Figure out which pages to show to the user.
        lower = max(1, min(page_count - 4, page - 2))
        pages = [(lower + x) for x in range(5) if (lower + x) <= page_count]

        # Fetch units for the included game ids.
        game_id_list = [g[x_id] for g in games]
        games = request.sandbox.recall(
             units.Game,
             lambda x: x.id in game_id_list
        )

        # Sort the page results to ensure that they are consistent with the global sort.
        if order == 'recent':
            games.sort(**{
                'key': lambda x: x.on_sale_since,
                'reverse': True,
            })
        elif order == 'savings':
            games.sort(**{
                'key': lambda x: (x.price - x.sale_price) / x.price,
                'reverse': True
            })
        elif order == 'gauge':
            games.sort(**{
                'key': lambda x: x.community_value,
            })
        elif order == 'hours':
            games.sort(**{
                'key': lambda x: (x.median_hours_played or 0, x.id),
                'reverse': True
            })
        else:
            games.sort(**{
                'key': lambda x: x.sale_price,
            })

        # Split the list into two lists for display
        games_one = []
        games_two = []

        for x in range(0, len(games)):
            if x % 2:
                games_two.append(games[x])
            else:
                games_one.append(games[x])

        return {
            'games_one': games_one,
            'games_two': games_two,
            'order': order,
            'page': page,
            'pages': pages,
            'page_count': page_count,
            'total_count': total_count,
            'genre': genre.name.lower() if genre else None,
            'genres': genres,
            'price': price,
        }

    #---------------------------------------------------------------------------
    @cherrypy.expose()
    @auth.tools.SigninRequired(redirect='/giveaway/')
    @cream.tools.JinjaTemplate('/gamestradamus/profile/giveaway.html')
    def giveaway(self, *args, **kwargs):
        request = cherrypy.serving.request

        now = timeutils.utcnow()
        local_dir = os.path.join(os.getcwd(), os.path.dirname(__file__))
        with open(os.path.join(local_dir, '..', '..', 'weekly-game-giveaway-schedule.csv')) as csvfile:
            reader = csv.reader(csvfile)
            reader.next()
            for row in reader:
                d = datetime.datetime.strptime(row[2], '%Y-%m-%d %H:%M:%S')
                d = d.replace(tzinfo=cream.timeutils.Timezone.from_cache(0))
                if d < now:
                    continue
                break

            id = int(row[1])

        rating_count = request.sandbox.count(
            units.UserAccountGame,
            lambda x: x.user_account_id == request.user_account.id and \
                x.rating > 0
        )
        emails = request.sandbox.recall(
            units.UserAccountEmail,
            lambda x: x.user_account_id == request.user_account.id and \
                x.watch_list_notifications == True
        )
        watches = request.sandbox.recall(
            units.UserAccountGameWatch,
            lambda x: x.user_account_id == request.user_account.id and \
                x.game_id == id
        )

        return {
            'game': request.sandbox.Game(id=id),
            'now': datetime.datetime.now(),
            'rating_count': rating_count,
            'emails': emails,
            'watches': watches,
        }
    # to allow for signed in/out redirecting, don't restrict this page to just welcomed.
    giveaway._cp_config = {'tools.welcome.on': False}

    #---------------------------------------------------------------------------
    @cherrypy.expose(alias='csv-export')
    @auth.tools.SigninRequired()
    def csv_export(self, *args, **kwargs):
        request = cherrypy.serving.request

        results = request.sandbox.recall(
            units.UserAccountGame << units.Game,
            lambda x, y: x.user_account_id == request.user_account.id \
                and (x.ignore == False or x.ignore is None),
        )
        results = sorted(results, key=lambda x: x[0].hours_played, reverse=True)

        user_account_games = [{
            'Game': 'Game',
            'Hours': 'Hours',
            'Cost': 'Cost',
            '$ / Hour': '$ / Hour',
            'Rating': 'Rating',
            'Finished': 'Finished',
            'Tags': 'Tags',
        }]
        for result in results:
            user_account_games.append({
                'Game': (u'%s' % result[1].name).encode('utf-8'),
                'Hours': (u'%.1f' % result[0].hours_played).encode('utf-8'),
                'Cost': (u'%.2f' % result[0].dollars_paid).encode('utf-8'),
                '$ / Hour': (u'%.2f' % (result[0].cost_per_hour or 0)).encode('utf-8'),
                'Rating': result[0].rating or 0,
                'Finished': 'Y' if result[0].finished else 'N',
                'Tags': result[0].tags.encode('utf-8') if result[0].tags else '',
            })

        out = StringIO.StringIO()
        writer = csv.DictWriter(
            out, ['Game', 'Cost', 'Hours', '$ / Hour', 'Rating', 'Finished', 'Tags']
        )
        writer.writerows(user_account_games)
        cherrypy.response.headers['Content-Type'] = 'text/csv'
        cherrypy.response.headers['Content-Length'] = out.len
        return out.getvalue()

    #---------------------------------------------------------------------------
    @cherrypy.expose()
    @auth.tools.SigninRequired()
    @cream.tools.JinjaTemplate('/gamestradamus/profile/play-list.html')
    def playlist(self, *args, **kwargs):
        request = cherrypy.serving.request

        results = request.sandbox.recall(
            units.UserAccountGame << units.Game,
            lambda x, y: x.user_account_id == request.user_account.id \
                and (x.finished == False or x.finished is None) \
                and (x.ignore == False or x.ignore is None)
        )
        results.sort(**{
            'key': lambda x: (
                -x[1].community_rating if not x[1].canonical_game_id else -x[1].Game().community_rating,
                -(x[1].median_hours_played or 0),
                x[1].name
            )
        })

        # Figure out which tags to show the user for filtering.
        user_account_tag_list = request.user_account.UserAccountTag()
        tag_group_lookup = dict([(x.name, x.group or None) for x in user_account_tag_list])
        group_tag_list = [(k, sorted(g, key=lambda x: x.name.lower())) for k, g in itertools.groupby(
            sorted(user_account_tag_list, key=lambda x: x.group or None),
            lambda x: x.group or None
        )]
        group_tag_list.sort(key=lambda x: x[0] if x[0] is not None else 'zzzzzzz')

        # Figure out which genres to show the user for filtering.
        genre_filter_list = request.sandbox.recall(
            units.Genre,
            lambda x: x.id < 14
        )

        context = {
            'results': results,
            'group_tag_list': group_tag_list,
            'color_hash': color_hash,
            'tag_group_lookup': tag_group_lookup,
            'genre_filter_list': genre_filter_list,
        }

        # This page should not be cached by browsers as it is extremely dynamic.
        cherrypy.response.headers['Cache-Control'] = 'no-cache'

        return context

    #---------------------------------------------------------------------------
    @cherrypy.expose()
    @auth.tools.SigninRequired()
    @cream.tools.JinjaTemplate('/gamestradamus/profile/watch-list.html')
    def watchlist(self, *args, **kwargs):
        request = cherrypy.serving.request
        user_attrs = request.user_account.UserAccountAttributes()

        results = request.sandbox.recall(
            units.UserAccountGameWatch << units.Game,
            lambda x, y: x.user_account_id == request.user_account.id
        )
        results.sort(**{
            'key': lambda x: (x[1].min_price, x[1].name, x[0].id),
        })

        # Add superscript assignments to multiple watches on the same game.
        for group, items in itertools.groupby(results, key=lambda x: x[1].id):
            items = list(items)
            if len(items) > 1:
                for x in range(len(items)):
                    items[x][0].superscript = x + 1

        emails = request.sandbox.recall(
            units.UserAccountEmail,
            lambda x: x.user_account_id == request.user_account.id and \
                x.watch_list_notifications == True
        )

        context = {
            'results': results,
            'has_watchlist_email': True if len(emails) else False,
            'user_attrs': user_attrs,
        }

        # This page should not be cached by browsers as it is extremely dynamic.
        cherrypy.response.headers['Cache-Control'] = 'no-cache'

        return context

    #---------------------------------------------------------------------------
    @cherrypy.expose()
    @auth.tools.SigninRequired()
    @cream.tools.JinjaTemplate('/gamestradamus/profile/settings.html')
    def settings(self, *args, **kwargs):
        request = cherrypy.serving.request

        user_attrs = request.user_account.UserAccountAttributes()
        emails = request.sandbox.recall(
            units.UserAccountEmail,
            lambda x: x.user_account_id == request.user_account.id,
            ["date_confirmed asc"]
        )
        context = {
            'user_account': request.user_account,
            'user_attrs': user_attrs,
            'emails': emails,
            'regions': REGIONS,
            'current_region': request.user_region,
            'tag_list': sorted(
                request.user_account.UserAccountTag(),
                key=lambda x: (x.group or None, x.name.lower())
            ),
            'friend_list': sorted(request.sandbox.recall(
                    units.UserAccountFriend << auth.units.UserAccount,
                    lambda x, y: x.user_account_id == request.user_account.id
                ),
                key=lambda x: (x[1].nickname,)
            ),
            'payments': request.user_account.UserAccountPayment(),
        }
        # This page should not be cached by browsers as it is extremely dynamic.
        cherrypy.response.headers['Cache-Control'] = 'no-cache'

        return context

    #---------------------------------------------------------------------------
    @cherrypy.expose()
    @auth.tools.SigninRequired()
    @cream.tools.JinjaTemplate('/gamestradamus/profile/forget.html')
    def forget(self, *args, **kwargs):
        """Forget a UserAccount and all associated data."""

        request = cherrypy.serving.request

        if request.method != 'POST':
            cherrypy.session['csrf-token'] = crypto.get_random_string(12)
            return {
                'csrf_token': cherrypy.session.get('csrf-token')
            }
        else:
            if kwargs.get('csrf-token', None) != cherrypy.session.get('csrf-token', crypto.get_random_string(12)):
                raise cherrypy.NotFound()

        user_account = request.user_account

        # Forget the game records in their Library.
        for x in user_account.UserAccountGame():
            x.forget()

        # Forget Achievements
        for x in user_account.UserAccountAchievement():
            x.forget()

        # Forget any Tags they have applied to their Library
        for x in user_account.UserAccountTag():
            x.forget()

        # Forget the friends and invitations that they have sent.
        friends = request.sandbox.recall(
            units.UserAccountFriend,
            lambda x: x.user_account_id == user_account.id or x.friend_id == user_account.id
        )
        for x in friends:
            x.forget()

        invitations = request.sandbox.recall(
            units.UserAccountInvitation,
            lambda x: x.user_account_id == user_account.id or x.friend_id == user_account.id
        )
        for x in invitations:
            x.forget()

        # Forget their Library statistics.
        user_account.UserAccountStatistics().forget()

        # Forget the Attributes tracked for the account.
        user_account.UserAccountAttributes().forget()

        # Drop everything in their Watchlist
        for x in user_account.UserAccountGameWatch():
            x.forget()

        # Any emails they have setup need to be forgotten.
        for x in user_account.UserAccountEmail():
            x.forget()

        # Finally forget the account record itself.
        user_account.forget()

        # Get rid of the csrf-token
        del cherrypy.session['csrf-token']

        # Log the account out.
        raise cherrypy.HTTPRedirect('/auth/signout/')

    #---------------------------------------------------------------------------
    @cherrypy.expose()
    @auth.tools.SigninRequired()
    @cream.tools.JinjaTemplate('/gamestradamus/profile/your-games.html')
    def games(self, *args, **kwargs):
        request = cherrypy.serving.request

        price_attr = request.user_region['game_attributes']['price']
        sale_price_attr = request.user_region['game_attributes']['sale_price']
        on_sale_since_attr = request.user_region['game_attributes']['on_sale_since']

        def flatten(x):
            """Convert raw view data into named dicts."""
            d = [{
                'id': x[0],
                'ignore': x[1],
                'tags': x[2],
                'corrected': x[3],
                'dollars_paid': x[4],
                'custom_hours_played': x[5],
                'steam_hours_played': x[6],
                'cost_per_hour': x[7],
                'rating': x[8],
                'finished': x[9],

                # Pre-compute the hours played value.
                'hours_played': (x[5] or 0) + (x[6] or 0)
            }, {
                'id': x[10],
                'canonical_game_id': x[11],
                'name': x[12],
                'logo': x[13],
                'price': x[14],
                'sale_price': x[15],
                'on_sale': True if x[16] is not None else False,
                'genre': x[17],
                'app_id': x[18]
            }]

            if x[11]:
                # The game is an alias for another canonical game. Pull up the
                # pricing data for the canonical game.
                data = request.sandbox.store.view((
                    units.Game,
                    lambda g: (
                        getattr(g, price_attr),
                        getattr(g, sale_price_attr),
                        getattr(g, on_sale_since_attr)
                    ),
                    lambda g: g.id == x[11]
                ))

                d[1]['price'] = data[0][0]
                d[1]['sale_price'] = data[0][1]
                d[1]['on_sale'] = True if data[0][2] is not None else False

            return d

        data = request.sandbox.store.view((
            units.UserAccountGame << units.Game,
            lambda uag, g: (
                uag.id,
                uag.ignore,
                uag.tags,
                uag.corrected,
                uag.dollars_paid,
                uag.custom_hours_played,
                uag.steam_hours_played,
                uag.cost_per_hour,
                uag.rating,
                uag.finished,

                g.id,
                g.canonical_game_id,
                g.name,
                g.logo,
                getattr(g, price_attr),
                getattr(g, sale_price_attr),
                getattr(g, on_sale_since_attr),
                g.genre,
                g.app_id
            ),
            lambda x, y: x.user_account_id == request.user_account.id \
                and (x.ignore == False or x.ignore is None)
        ))
        results = [flatten(x) for x in data]
        results.sort(**{
            'key': lambda x: x[0]['hours_played'],
            'reverse': True
        })

        user_attrs = request.user_account.UserAccountAttributes()
        allow_update = False
        if (
            user_attrs and
            user_attrs.games_last_updated < (timeutils.utcnow() - datetime.timedelta(minutes=1))
        ):
            allow_update = True

        region = REGIONS[user_attrs.region]

        # data required to display and filter by custom tags.
        user_account_tag_list = request.user_account.UserAccountTag()
        tag_group_lookup = dict([(x.name, x.group or None) for x in user_account_tag_list])
        group_tag_list = [(k, sorted(g, key=lambda x: x.name.lower())) for k, g in itertools.groupby(
            sorted(user_account_tag_list, key=lambda x: x.group or None),
            lambda x: x.group or None
        )]
        group_tag_list.sort(key=lambda x: x[0] if x[0] is not None else 'zzzzzzz')

        # Look up any friends that this profile may have.
        friend_list = [x[1] for x in request.sandbox.xrecall(
            units.UserAccountFriend << auth.units.UserAccount,
            lambda x, y: x.user_account_id == request.user_account.id
        )]

        # Figure out which genres to show the user for filtering.
        genre_filter_list = request.sandbox.recall(
            units.Genre,
            lambda x: x.id < 14
        )

        rating_filter_list = [
            {'label': u'★★★★★', 'filter': '5'},
            {'label': u'★★★★', 'filter': '4'},
            {'label': u'★★★', 'filter': '3'},
            {'label': u'★★', 'filter': '2'},
            {'label': u'★', 'filter': '1'},
            {'label': u'Unrated', 'filter': '0'},
        ]
        finished_filter_list = [
            u'Yes',
            u'No',
        ]
        cost_filter_list = region['cost_filter_list']
        hours_filter_list = [
            {'label': '< 1 hr', 'filter': '0-1'},
            {'label': '1 - 8 hrs', 'filter': '1-8'},
            {'label': '8 - 20 hrs', 'filter': '8-20'},
            {'label': '20 - 40 hrs', 'filter': '20-40'},
            {'label': '40+ hrs', 'filter': '40-100000'}
        ]

        context = {
            'user_account': request.user_account,
            'last_updated': Converters.iso_from_datetime(user_attrs.games_last_updated),
            'allow_update': allow_update,
            'user_attrs': user_attrs,
            'results': results,
            'color_hash': color_hash,
            'tag_group_lookup': tag_group_lookup,
            'group_tag_list': group_tag_list,
            'rating_filter_list': rating_filter_list,
            'finished_filter_list': finished_filter_list,
            'genre_filter_list': genre_filter_list,
            'cost_filter_list': cost_filter_list,
            'hours_filter_list': hours_filter_list,
            'friend_list': friend_list,

            'preserve_whitespace': True
        }

        # This page should not be cached by browsers as it is extremely dynamic.
        cherrypy.response.headers['Cache-Control'] = 'no-cache'

        return context

    #---------------------------------------------------------------------------
    @cherrypy.expose()
    @auth.tools.SigninRequired()
    @cream.tools.JinjaTemplate('/gamestradamus/profile/your-ignored-games.html')
    def ignored(self, *args, **kwargs):
        request = cherrypy.serving.request

        zero = decimal.Decimal('0.0')
        results = request.sandbox.recall(
            units.UserAccountGame << units.Game,
            lambda x, y: x.user_account_id == request.user_account.id and x.ignore == True
        )
        results.sort(**{
            'key': lambda x: (
                x[0].steam_hours_played if x[0].steam_hours_played else zero
            ) + (
                x[0].custom_hours_played if x[0].custom_hours_played else zero
            ),
            'reverse': True
        })

        context = {
            'results': results,
            'color_hash': color_hash,
        }
        return context

    #---------------------------------------------------------------------------
    @cherrypy.expose()
    @auth.tools.SigninRequired()
    @cream.tools.JinjaTemplate('/gamestradamus/profile/insights.html')
    def insights(self, *args, **kwargs):
        request = cherrypy.serving.request

        tags = request.user_account.UserAccountTag()
        tags = sorted(tags, key=lambda x: x.group or 'zzzzzz')
        tag_groups = []
        for k, g in itertools.groupby(tags, key=lambda x: x.group or None):
            tag_groups.append({'name': k, 'tags': list(g)})

        def flatten(game):
            tags = collections.defaultdict(list)
            if game[8]: # if game.tags:
                for tag in game[8].split('|'):
                    pieces = tag.split(' / ')
                    if len(pieces) > 1:
                        tags['Tag/%s' % pieces[0]].append({
                            'label': pieces[1],
                            'filter': tag,
                        })

            genres = []
            if game[2]: # if game.genre
                for g in game[2].split(', '):
                    genres.append({
                        'label': g,
                        'filter': g
                    })

            return {
                'id': game[0],
                'n': game[1],
                'g': genres,
                'hp': float('%.1f' % ((game[3] or 0) + (game[4] or 0))),    # hours-played
                'dp': float('%.2f' % (game[5] or 0)),                       # dollars-paid
                'f': game[6] or False,                                      # finished
                'r': game[7] or 0,                                          # rating
                't': tags,                                                  # tags
            }
        data = request.sandbox.store.view((
            units.UserAccountGame << units.Game,
            lambda x, y: (
                y.id,
                y.name,
                y.genre,
                x.custom_hours_played,
                x.steam_hours_played,
                x.dollars_paid,
                x.finished,
                x.rating,
                x.tags
            ),
            lambda x, y: x.user_account_id == request.user_account.id \
                and (x.ignore == False or x.ignore is None)
        ))
        data = [flatten(x) for x in data]

        # Look up any friends that this profile may have.
        friend_list = [x[1] for x in request.sandbox.xrecall(
            units.UserAccountFriend << auth.units.UserAccount,
            lambda x, y: x.user_account_id == request.user_account.id
        )]
        user_attrs = request.user_account.UserAccountAttributes()

        return {
            'tag_groups': tag_groups,
            'data': data,
            'user_attrs': user_attrs,
            'friend_list': friend_list,
        }

    #---------------------------------------------------------------------------
    @cherrypy.expose()
    @cream.tools.JinjaTemplate('/gamestradamus/profile/game.html')
    def game(self, game_id, *args, **kwargs):
        request = cherrypy.serving.request

        # If the user asking for this page is not logged in, send them
        # to the equivalent logged out version for this game_id.
        auth_check = auth.tools.SigninRequired(redirect='/game/%s/' % game_id)
        auth_check(lambda: True)()

        game = request.sandbox.Game(**{'id': game_id})
        if game is None:
            raise cherrypy.NotFound()
        if game.canonical_game_id:
            raise cherrypy.HTTPRedirect(
                '/profile/game/%s/' % game.canonical_game_id
            )

        # If the user already owns this game, indicate that to the template.
        # Remember to take into account canonical game relationships.
        user_account_games = request.sandbox.recall(
                units.UserAccountGame << units.Game,
                lambda uag, g: (
                    (g.id == game.id or g.canonical_game_id == game.id) and
                    uag.user_account_id == request.user_account.id
                )
        )
        if user_account_games:
            user_account_game = user_account_games[0][0]
        else:
            user_account_game = None

        # If the user has this game on their watch list, indicate that to the
        # template
        watch = request.sandbox.UserAccountGameWatch(**{
            'user_account_id': request.user_account.id,
            'game_id': game.id,
        })

        return {
            'game': game,
            'user_account': request.user_account,
            'user_attrs': request.user_account.UserAccountAttributes(),
            'user_account_game': user_account_game,
            'watch': watch,
            'now': datetime.datetime.now(),
        }

    # To allow for game redirecting, don't restrict this page to just welcomed
    game._cp_config = {'tools.welcome.on': False}

#-------------------------------------------------------------------------------
class AuthenticatedCUDLHandler(rpc.dejavu.CUDLHandler):
    """Require Authentication on top of CUDL Handler features.

    Unit is required to have an attribute of 'user_account_id'
    """

    #---------------------------------------------------------------------------
    @cherrypy.expose()
    @auth.handlers.Authenticated()
    def list(self, *args, **kwargs):
        return super(AuthenticatedCUDLHandler, self).list(*args, **kwargs)

    #---------------------------------------------------------------------------
    @cherrypy.expose()
    @auth.handlers.Authenticated()
    def update(self, *args, **kwargs):
        return super(AuthenticatedCUDLHandler, self).update(*args, **kwargs)

    #---------------------------------------------------------------------------
    @cherrypy.expose()
    @auth.handlers.Authenticated()
    def delete(self, *args, **kwargs):
        return super(AuthenticatedCUDLHandler, self).delete(*args, **kwargs)

    #---------------------------------------------------------------------------
    @cherrypy.expose()
    @auth.handlers.Authenticated()
    def create(self, *args, **kwargs):
        return super(AuthenticatedCUDLHandler, self).create(*args, **kwargs)

    #---------------------------------------------------------------------------
    def _before_update(self, items, values):
        user_account = cherrypy.serving.request.user_account

        items = super(AuthenticatedCUDLHandler, self)._before_update(items, values)
        items = [item for item in items if item.user_account_id == user_account.id]
        return items

    #---------------------------------------------------------------------------
    def _before_delete(self, items):
        user_account = cherrypy.serving.request.user_account

        items = super(AuthenticatedCUDLHandler, self)._before_delete(items)
        items = [item for item in items if item.user_account_id == user_account.id]
        return items

    #---------------------------------------------------------------------------
    def _after_list(self, items):
        user_account = cherrypy.serving.request.user_account

        items = super(AuthenticatedCUDLHandler, self)._after_list(items)
        items = [item for item in items if item.user_account_id == user_account.id]
        return items

    #---------------------------------------------------------------------------
    def _pre_create(self, query, values):
        user_account = cherrypy.serving.request.user_account
        values['user_account_id'] = user_account.id
        return query, values

#-------------------------------------------------------------------------------
def validate_user_account_game_id(value):
    request = cherrypy.serving.request
    user_account = request.user_account

    # Ensure the game they are adding actually exists.
    game = request.sandbox.unit(units.Game, **{
        'id': value,
    })
    if game is None:
        raise form.ValidateError('Unrecognized game')

    # Make an effort to prevent them from adding a game they already have
    # in their library, being careful to take into account the canonical_game_id
    # relationship
    # NOTE: there is a race condition here. If they we are processing their library
    # while they are adding the game at the same time, we might end up with two.
    user_account_games = request.sandbox.recall(
            units.UserAccountGame << units.Game,
            lambda uag, g: (
                (g.id == game.id or g.canonical_game_id == game.id)
                and uag.user_account_id == request.user_account.id
            )
        )
    if user_account_games:
        raise form.ValidateError('%s is already in your library' % (game.name,))


#-------------------------------------------------------------------------------
class AuthenticatedUserAccountGameCUDLHandler(AuthenticatedCUDLHandler):

    units = (units.UserAccountGame,)
    expand_lookup = {}

    create_rql = rql.Validator()
    create_schema = rpc.Schema(rpc.Type.Struct, {
        'game_id': rpc.Schema(
            rpc.Type.Integer,
            validators=[
                form.Validator.positivenumber(),
                validate_user_account_game_id,
            ],
        ),
    })

    update_rql = rql.Validator(
        variables={
            'id': rql.Number(required=True),
        }
    )
    update_schema = rpc.Schema(rpc.Type.Struct, {
        'custom_hours_played': rpc.Schema(
            rpc.Type.Decimal,
        ),
        'dollars_paid': rpc.Schema(
            rpc.Type.Decimal,
            validators=[
                form.Validator.positivenumber()
            ]
        ),
        'cost_per_hour': rpc.Schema(
            rpc.Type.Decimal,
            validators=[
                form.Validator.positivenumber()
            ]
        ),
        'rating': rpc.Schema(
            rpc.Type.Integer,
            validators=[
                form.Validator.positivenumber(),
                form.Validator.max(5),
                form.Validator.min(1)
            ],
            null=True
        ),
        'ignore': rpc.Schema(rpc.Type.Boolean),
        'corrected': rpc.Schema(rpc.Type.Boolean),
        'finished': rpc.Schema(rpc.Type.Boolean),
        'tags': rpc.Schema(
            rpc.Type.String,
            validators=[
                form.Validator.notempty(),
                form.Validator.websafe()
            ],
            null=True
        )
    })

    #---------------------------------------------------------------------------
    @cherrypy.expose()
    @auth.handlers.Authenticated()
    def create(self, *args, **kwargs):
        return super(AuthenticatedUserAccountGameCUDLHandler, self).create(*args, **kwargs)

    #---------------------------------------------------------------------------
    def _pre_create(self, query, values):
        query, values = super(AuthenticatedUserAccountGameCUDLHandler, self)._pre_create(query, values)
        values['added_by_user'] = True
        return query, values

    #---------------------------------------------------------------------------
    def _post_create(self, items):
        request = cherrypy.serving.request
        for uag in items:
            uag.set_initial_price(request.user_region['value'])
        return items

#-------------------------------------------------------------------------------
def validate_watch_game_id(value):
    request = cherrypy.serving.request
    game = request.sandbox.unit(units.Game, **{
        'id': value,
    })
    if game is None:
        raise form.ValidateError('Unrecognized game')

#-------------------------------------------------------------------------------
def validate_watch_operation(value):
    if value not in ('price', 'cost-per-hour'):
        raise form.ValidateError(
            'Unrecognized operation, try "price" or "cost-per-hour"'
        )

#-------------------------------------------------------------------------------
class AuthenticatedUserAccountGameWatchCUDLHandler(AuthenticatedCUDLHandler):

    expand_lookup = {
            'game': units.Game,
        }

    units = (units.UserAccountGameWatch,)

    create_rql = rql.Validator(
            expand=['game']
        )
    create_schema = rpc.Schema(rpc.Type.Struct, {
        'game_id': rpc.Schema(
            rpc.Type.Integer,
            validators=[
                form.Validator.positivenumber(),
                validate_watch_game_id,
            ],
            required=True,
        ),
    })
    delete_rql = rql.Validator(
            variables={
                'id': rql.Number(required=True),
            }
        )
    update_rql = rql.Validator(
        variables={
            'id': rql.Number(required=True),
        }
    )
    update_schema = rpc.Schema(rpc.Type.Struct, {
        'limit': rpc.Schema(
            rpc.Type.Decimal,
            null=True,
            validators=[
                form.Validator.positivenumber(),
            ]
        ),
        'operation': rpc.Schema(
            rpc.Type.String,
            null=True,
            validators=[
                validate_watch_operation
            ]
        ),
    })

#-------------------------------------------------------------------------------
class AuthenticatedUserAccountEmailCUDLHandler(AuthenticatedCUDLHandler):

    expand_lookup = {}

    units = (units.UserAccountEmail,)

    create_rql = rql.Validator()
    create_schema = rpc.Schema(rpc.Type.Struct, {
        'email': rpc.Schema(
            rpc.Type.String,
            validators=[
                form.Validator.email(),
                plumbing_email.handlers.email_is_subscribed,
            ],
            required=True,
        ),
        'marketing_notifications': rpc.Schema(
            rpc.Type.Boolean,
        )
    })

    delete_rql = rql.Validator(
        variables={
            'id': rql.Number(required=True),
        }
    )

    update_rql = rql.Validator(
        variables={
            'id': rql.Number(required=True),
        }
    )
    update_schema = rpc.Schema(rpc.Type.Struct, {
        'watch_list_notifications': rpc.Schema(
            rpc.Type.Boolean,
        ),
        'marketing_notifications': rpc.Schema(
            rpc.Type.Boolean,
        ),
    })

    #---------------------------------------------------------------------------
    def _post_create(self, items):
        items = super(AuthenticatedUserAccountEmailCUDLHandler, self)._post_create(items)
        request = cherrypy.serving.request
        code_size = request.config.get('gauge.random_code_size')
        for item in items:
            item.set_defaults(code_size)
            self._send_confirmation_email(item)
        return items

    #---------------------------------------------------------------------------
    def _send_confirmation_email(self, user_account_email):
        """Sends the confirmation email for confirming that you own this email
        """
        request = cherrypy.serving.request
        context = {
            'email': user_account_email,
            'user_account': request.user_account,
        }

        email.render_add_to_email_queue(**{
            'to_address': user_account_email.email,
            'email_name': 'confirm-email-address',
            'context': context,
            'options': '{ "tags": ["confirm-add-email-address"] }',
            'sandbox': request.sandbox,
        })

    #---------------------------------------------------------------------------
    @cherrypy.expose(alias='resend-confirmation-email')
    @rpc.Signature(rpc.Type.Array, [('query', rpc.Type.String)])
    @auth.handlers.Authenticated()
    def resend_confirmation_email(self, query, **kwargs):
        user_account = cherrypy.serving.request.user_account
        box = cherrypy.serving.request.sandbox

        validator = rql.Validator(
            variables={
                'id': rql.Number(required=True),
            }
        )

        try:
            query = validator.validate(pyrqlate.parse(query))
        except (form.ValidateError, pyrqlate.ParseError), e:
            raise rpc.Error(-65336, unicode(e))

        items = self._get(query, box)
        items = [item for item in items if item.user_account_id == user_account.id]

        for item in items:
            self._send_confirmation_email(item)

        # Apply RQL selection and expansion.
        items = self._apply_rql(query, items, box)

        return items

valid_region = form.Validator.choice(
        [region for region, settings in REGIONS.iteritems()]
    )
#-------------------------------------------------------------------------------
class AuthenticatedUserAccountAttributesCUDLHandler(AuthenticatedCUDLHandler):
    expand_lookup = {}

    units = (units.UserAccountAttributes,)

    # We allow you to update your data usage opt out
    update_rql = rql.Validator(
        variables={
            'id': rql.Number(required=True, count=1),
        }
    )
    update_schema = rpc.Schema(rpc.Type.Struct, {
        'data_usage_opt_out': rpc.Schema(
            rpc.Type.Datetime,
            null=True,
            required=False,
        ),
        'has_ignored_watchlist_email': rpc.Schema(
            rpc.Type.Boolean,
            null=True,
            required=False,
        ),
        'region': rpc.Schema(
            rpc.Type.String,
            null=True,
            required=False,
            validators=[
                valid_region,
            ],
        ),
    })

    #---------------------------------------------------------------------------
    def _before_update(self, items, values):
        for item in items:
            item._old_region_cache = item.region
        return items

    #---------------------------------------------------------------------------
    def _after_update(self, items):
        for item in items:
            if item._old_region_cache != item.region:
                self._update_account_games_default_price(item)
        return items

    #---------------------------------------------------------------------------
    def _update_account_games_default_price(self, user_attributes):
        """Whenever the user switches their region, we will go through
        and change the uncorrected pricing on their games.
        """
        for uag, g in user_attributes.sandbox.xrecall(
                units.UserAccountGame << units.Game,
                lambda uag, g: uag.corrected != True and uag.user_account_id == user_attributes.user_account_id
            ):
            # Because of the way we get the users "region"
            # we can't assume that game.price will use the right one
            # explicitly ask for the new region's price.
            uag.dollars_paid = g.canonical_game.get_price(user_attributes.region)
            uag.update_cost_per_hour()

#-------------------------------------------------------------------------------
def tag_name_group_validator(value, exclude=[]):
    """Ensure that no tag can have the same name & group."""
    request = cherrypy.serving.request

    name = value.get('name')
    if '/' in name or '|' in name or '_' in name:
        raise form.ValidateError(
            u'name: You cannot use "/" or "|" or "_" in a tag name.'
        )

    group = value.get('group', None)
    if group and ('/' in group or '|' in group or '_' in name):
        raise form.ValidateError(
            u'group: You cannot use "/" or "|" or "_" in a group name.'
        )

    tags = request.sandbox.xrecall(
        units.UserAccountTag,
        lambda x: x.user_account_id == request.user_account.id and
            x.id not in exclude
    )
    for tag in tags:
        if tag.name == name and tag.group == group:
            if group:
                raise form.ValidateError(
                    u'name: You already have a tag named "%s" within group "%s". Please choose another name.' % (name, group)
                )
            else:
                raise form.ValidateError(
                    u'name: You already have a tag named "%s". Please choose another name.' % name
                )

#-------------------------------------------------------------------------------
class AuthenticatedUserAccountTagCUDLHandler(AuthenticatedCUDLHandler):
    expand_lookup = {}
    units = (units.UserAccountTag,)

    create_rql = rql.Validator()
    create_schema = rpc.Schema(rpc.Type.Struct, {
        'name': rpc.Schema(
            rpc.Type.String,
            validators=[
                form.Validator.notempty(),
                form.Validator.websafe(message=u'You cannot use "%(leftovers)s" in a tag name.'),
            ],
            required=True
        ),
        'group': rpc.Schema(
            rpc.Type.String,
            validators=[
                form.Validator.notempty(),
                form.Validator.websafe(message=u'You cannot use "%(leftovers)s" in a group name.'),
            ],
            null=True
        )
    }, validators=[
        tag_name_group_validator,
    ])

    delete_rql = rql.Validator(
        variables={
            'id': rql.Number(required=True, count=1)
        }
    )

    update_rql = rql.Validator(
        variables={
            'id': rql.Number(required=True, count=1)
        }
    )
    update_schema = rpc.Schema(rpc.Type.Struct, {
        'name': rpc.Schema(
            rpc.Type.String,
            validators=[
                form.Validator.notempty(),
                form.Validator.websafe(),
                # The tag name validator is called in the _before_update hook.
            ],
        ),
        'group': rpc.Schema(
            rpc.Type.String,
            validators=[
                form.Validator.notempty(),
                form.Validator.websafe(),
            ],
            null=True
        )
    })

    #---------------------------------------------------------------------------
    def _after_delete(self, items):
        """Remove tag from the user_account's UserAccountGame record."""

        request = cherrypy.serving.request
        uag_list = request.user_account.UserAccountGame()
        for item in items:
            for uag in uag_list:
                if uag.tags:
                    tag_name_list = uag.tags.split('|')
                    tag_name_list = [x for x in tag_name_list if x != item.qualified_name]
                    uag.tags = '|'.join(tag_name_list)

        return items

    #---------------------------------------------------------------------------
    def _before_update(self, items, values):
        """Update the tags on UserAccountGame records when the name changes."""

        def qualified_tag_name(group, name):
            if group:
                return ' / '.join([group, name])
            else:
                return name

        name = values.get('name', None)
        if not name:
            return
        group = values.get('group', None)

        request = cherrypy.serving.request
        uag_list = request.user_account.UserAccountGame()
        for item in items:
            if item.name == name and item.group == group:
                continue

            tag_name_group_validator({'name': name, 'group': group}, exclude=[item.id])

            for uag in uag_list:
                if uag.tags:
                    tag_name_list = uag.tags.split('|')
                    tag_name_list = [x if x != item.qualified_name else qualified_tag_name(group, name) for x in tag_name_list]
                    uag.tags = '|'.join(tag_name_list)

        return items

#-------------------------------------------------------------------------------
class AuthenticatedUserAccountInvitationCUDLHandler(AuthenticatedCUDLHandler):
    """
    Create new invitations to become friends.
    """

    units = (units.UserAccountInvitation,)

    create_rql = rql.Validator()
    create_schema = rpc.Schema(rpc.Type.Struct, {
        'email': rpc.Schema(
            rpc.Type.String,
            validators=[
                form.Validator.email(),
            ],
            required=True
        )
    })

    #---------------------------------------------------------------------------
    def _post_create(self, items):
        items = super(AuthenticatedUserAccountInvitationCUDLHandler, self)._post_create(items)
        request = cherrypy.serving.request
        code_size = request.config.get('gauge.random_code_size')
        for item in items:
            item.set_defaults(code_size)
            self._send_invitation_email(item)
        return items

    #---------------------------------------------------------------------------
    def _send_invitation_email(self, user_account_invitation):
        """Sends the invitation email for becoming a friend with another account.
        """

        request = cherrypy.serving.request
        context = {
            'invitation': user_account_invitation,
            'user_account': request.user_account,
        }

        email.render_add_to_email_queue(**{
            'to_address': user_account_invitation.email,
            'email_name': 'friend-invitation',
            'context': context,
            'options': '{ "tags": ["friend-invitation"] }',
            'sandbox': request.sandbox,
        })

#-------------------------------------------------------------------------------
class AuthenticatedUserAccountFriendCUDLHandler(AuthenticatedCUDLHandler):
    """API Access for modification to UserAccountFriend records.
    """

    units = (units.UserAccountFriend,)

    delete_rql = rql.Validator(
        variables={
            'id': rql.Number(required=True, count=1)
        }
    )

    #---------------------------------------------------------------------------
    def _before_delete(self, items):
        """For each record that is being deleted, add the inverse relationship."""
        request = cherrypy.serving.request

        expanded = []
        items = super(AuthenticatedUserAccountFriendCUDLHandler, self)._before_delete(items)
        for item in items:
            expanded.append(item)

            inverse = request.sandbox.UserAccountFriend(**{
                'user_account_id': item.friend_id,
                'friend_id': item.user_account_id,
            })
            if inverse is not None:
                expanded.append(inverse)

        return expanded

