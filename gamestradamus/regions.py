# coding: utf-8
# TODO: we could accomplish this with proper "locale" support, but we're
#       not (currently) supporting locales across the board, so I'm not 
#       going to bother.
from decimal import Decimal

ONE_DOLLAR = Decimal("1.00")
ZERO = Decimal("0.00")

#-------------------------------------------------------------------------------
class RegionalNumber(object):

    #---------------------------------------------------------------------------
    @classmethod
    def format_owner_count(cls, owners):
        """Owner count is a whole number with appropriate thousands separator
        """
        if owners is None:
            owners = ZERO
        retval = (cls.owner_format % owners).replace(".", cls.radix)
        return retval

    #---------------------------------------------------------------------------
    @classmethod
    def format_hours(cls, hours):
        """Hours only come with one decimal point of precision and no unit.
        """
        if hours is None:
            hours = ZERO
        retval = (cls.hours_format % hours).replace(".", cls.radix)
        return retval

    #---------------------------------------------------------------------------
    @classmethod
    def format_cost_per_hour(cls, cost_per_hour, simplify=False):
        """Cost per hour."""
        if cost_per_hour is None:
            cost_per_hour = ZERO

        if cost_per_hour < ONE_DOLLAR and simplify:
            retval = '%.0f' % (cost_per_hour * 100)
        else:
            retval = (cls.cost_per_hour_format % cost_per_hour).replace('.', cls.radix)
        return retval

    #---------------------------------------------------------------------------
    @classmethod
    def format_price(cls, price, with_symbol=False):
        """Price is different from value.

        Prices are consistently displayed in the dollar unit. (Rather than
        in the cents unit).
        """
        if price is None:
            price = ZERO

        retval = (cls.dollars_format % price).replace(".", cls.radix)

        if with_symbol:
            if cls.dollar_symbol_position == "after":
                return retval + cls.dollar_symbol
            else:
                return cls.dollar_symbol + retval
        return retval

    #---------------------------------------------------------------------------
    @classmethod
    def format_rating(cls, rating):
        return (cls.rating_format % rating).replace(".", cls.radix)

#-------------------------------------------------------------------------------
class USD(RegionalNumber):
    dollar_symbol = u"$"
    cents_symbol = u"¢"
    dollar_symbol_no_html = dollar_symbol 
    dollar_symbol_position = "before"
    cents_symbol_position = "after"

    radix = "."
    currency_abbreviation = "USD"

    owner_format = u"%d"
    hours_format = u"%.1f"
    cents_format = u"%d"
    dollars_format = u"%.2f"
    cost_per_hour_format = u"%.2f"
    rating_format = u"%.1f"
    thousands_separator = ","

#-------------------------------------------------------------------------------
class BRL(RegionalNumber):
    dollar_symbol = u"R$"
    cents_symbol = u"¢"
    dollar_symbol_no_html = dollar_symbol 
    dollar_symbol_position = "before"
    cents_symbol_position = "after"

    radix = ","
    currency_abbreviation = "BRL"

    owner_format = u"%d"
    hours_format = u"%.1f"
    cents_format = u"%d"
    dollars_format = u"%.2f"
    cost_per_hour_format = u"%.2f"
    rating_format = u"%.1f"
    thousands_separator = "."

#-------------------------------------------------------------------------------
class GBP(RegionalNumber):
    dollar_symbol = u'£'
    cents_symbol = u'¢'
    dollar_symbol_no_html = dollar_symbol 
    dollar_symbol_position = "before"
    cents_symbol_position = "after"

    radix = "."
    currency_abbreviation = "GBP"

    owner_format = u"%d"
    hours_format = u"%.1f"
    cents_format = u"%d"
    dollars_format = u"%.2f"
    cost_per_hour_format = u"%.2f"
    rating_format = u"%.1f"
    thousands_separator = ","

#-------------------------------------------------------------------------------
class EUR(RegionalNumber):
    dollar_symbol = u'€'
    cents_symbol = u'c'
    dollar_symbol_no_html = dollar_symbol 
    dollar_symbol_position = "after"
    cents_symbol_position = "after"

    radix = ","
    currency_abbreviation = "EUR"

    owner_format = u"%d"
    hours_format = u"%.1f"
    cents_format = u"%d"
    dollars_format = u"%.2f"
    cost_per_hour_format = u"%.2f"
    rating_format = u"%.1f"
    thousands_separator = "&nbsp;"

#-------------------------------------------------------------------------------
class RUB(RegionalNumber):
    dollar_symbol = u' <span class="ruble-sign">pуб.</span>'
    cents_symbol = u' коп.'
    dollar_symbol_no_html = u'pуб.'
    dollar_symbol_position = "before"
    cents_symbol_position = "after"

    radix = ","
    currency_abbreviation = "RUB"

    owner_format = u"%d"
    hours_format = u"%.1f"
    cents_format = u"%d"
    dollars_format = u"%.0f"
    cost_per_hour_format = u"%.1f"
    rating_format = u"%.1f"
    thousands_separator = "&nbsp;"

REGIONS = {

    #-------------------------------------------------------------------------------
    'us': {
        'value': "us",
        'region_name': "United States, Canada and elsewhere",
        'country_abbreviation': u'us',
        'game_attributes': {
            'price': 'us_price',
            'sale_price': 'us_sale_price',
            'on_sale_since': 'us_on_sale_since',
            'available_since': 'us_available_since',
        },
        'numbers': USD,
        'cost_filter_list': [
            {'label': '< $5', 'filter': '0-5'},
            {'label': '$5 - $10', 'filter': '5-10'},
            {'label': '$10 - $20', 'filter': '10-20'},
            {'label': '$20 - $40', 'filter': '20-40'},
            {'label': '$40+', 'filter': '40-10000'}
        ],
    },

    #-------------------------------------------------------------------------------
    'uk': {
        'value': "uk",
        'region_name': "United Kingdom",
        'country_abbreviation': u'uk',
        'game_attributes': {
            'price': 'uk_price',
            'sale_price': 'uk_sale_price',
            'on_sale_since': 'uk_on_sale_since',
            'available_since': 'uk_available_since',
        },
        'numbers': GBP,
        'cost_filter_list': [
            {'label': u'< £5', 'filter': u'0-5'},
            {'label': u'£5 - £10', 'filter': u'5-10'},
            {'label': u'£10 - £20', 'filter': u'10-20'},
            {'label': u'£20 - £40', 'filter': u'20-40'},
            {'label': u'£40+', 'filter': u'40-10000'}
        ],
    },

    #-------------------------------------------------------------------------------
    'eu': {
        'value': "eu",
        'region_name': "European Union Region #1",
        'country_abbreviation': u'nl',
        'game_attributes': {
            'price': 'eu_price',
            'sale_price': 'eu_sale_price',
            'on_sale_since': 'eu_on_sale_since',
            'available_since': 'eu_available_since',
        },
        'numbers': EUR,
        'cost_filter_list': [
            {'label': u'< 5€', 'filter': u'0-5'},
            {'label': u'5€ - 10€', 'filter': u'5-10'},
            {'label': u'10€ - 20€', 'filter': u'10-20'},
            {'label': u'20€ - 40€', 'filter': u'20-40'},
            {'label': u'40+€', 'filter': u'40-10000'}
        ],
    },

    #-------------------------------------------------------------------------------
    'eu2': {
        'value': "eu2",
        'region_name': "European Union Region #2",
        'country_abbreviation': u'it',
        'game_attributes': {
            'price': 'eu2_price',
            'sale_price': 'eu2_sale_price',
            'on_sale_since': 'eu2_on_sale_since',
            'available_since': 'eu2_available_since',
        },
        'numbers': EUR,
        'cost_filter_list': [
            {'label': u'< 5€', 'filter': u'0-5'},
            {'label': u'5€ - 10€', 'filter': u'5-10'},
            {'label': u'10€ - 20€', 'filter': u'10-20'},
            {'label': u'20€ - 40€', 'filter': u'20-40'},
            {'label': u'40+€', 'filter': u'40-10000'}
        ],
    },

    #-------------------------------------------------------------------------------
    'de': {
        'value': "de",
        'region_name': "Germany",
        'country_abbreviation': u'de',
        'game_attributes': {
            'price': 'de_price',
            'sale_price': 'de_sale_price',
            'on_sale_since': 'de_on_sale_since',
            'available_since': 'de_available_since',
        },
        'numbers': EUR,
        'cost_filter_list': [
            {'label': u'< 5€', 'filter': u'0-5'},
            {'label': u'5€ - 10€', 'filter': u'5-10'},
            {'label': u'10€ - 20€', 'filter': u'10-20'},
            {'label': u'20€ - 40€', 'filter': u'20-40'},
            {'label': u'40+€', 'filter': u'40-10000'}
        ],
    },

    #-------------------------------------------------------------------------------
    'au': {
        'value': "au",
        'region_name': "Australia and New Zealand",
        'country_abbreviation': u'au',
        'game_attributes': {
            'price': 'au_price',
            'sale_price': 'au_sale_price',
            'on_sale_since': 'au_on_sale_since',
            'available_since': 'au_available_since',
        },
        # Yes, USD is the currency steam uses for Australia and New Zealand ?!
        'numbers': USD,
        'cost_filter_list': [
            {'label': '< $5', 'filter': '0-5'},
            {'label': '$5 - $10', 'filter': '5-10'},
            {'label': '$10 - $20', 'filter': '10-20'},
            {'label': '$20 - $40', 'filter': '20-40'},
            {'label': '$40+', 'filter': '40-10000'}
        ],
    },
    #-------------------------------------------------------------------------------
    'jp': {
        'value': "jp",
        'region_name': "Japan",
        'country_abbreviation': u'jp',
        'game_attributes': {
            'price': 'jp_price',
            'sale_price': 'jp_sale_price',
            'on_sale_since': 'jp_on_sale_since',
            'available_since': 'jp_available_since',
        },
        # Yes, USD is the currency steam uses for Japan
        'numbers': USD,
        'cost_filter_list': [
            {'label': '< $5', 'filter': '0-5'},
            {'label': '$5 - $10', 'filter': '5-10'},
            {'label': '$10 - $20', 'filter': '10-20'},
            {'label': '$20 - $40', 'filter': '20-40'},
            {'label': '$40+', 'filter': '40-10000'}
        ],
    },

    #-------------------------------------------------------------------------------
    'mx': {
        'value': "mx",
        'region_name': "Mexico",
        'country_abbreviation': u'mx',
        'game_attributes': {
            'price': 'mx_price',
            'sale_price': 'mx_sale_price',
            'on_sale_since': 'mx_on_sale_since',
            'available_since': 'mx_available_since',
        },
        # Yes, USD is the currency steam uses for Mexico
        'numbers': USD,
        'cost_filter_list': [
            {'label': '< $5', 'filter': '0-5'},
            {'label': '$5 - $10', 'filter': '5-10'},
            {'label': '$10 - $20', 'filter': '10-20'},
            {'label': '$20 - $40', 'filter': '20-40'},
            {'label': '$40+', 'filter': '40-10000'}
        ],
    },

    #-------------------------------------------------------------------------------
    'ua': {
        'value': "ua",
        'region_name': "Ukraine",
        'country_abbreviation': u'ua',
        'game_attributes': {
            'price': 'ua_price',
            'sale_price': 'ua_sale_price',
            'on_sale_since': 'ua_on_sale_since',
            'available_since': 'ua_available_since',
        },
        # Yes, USD is the currency steam uses for Ukraine
        'numbers': USD,
        'cost_filter_list': [
            {'label': '< $5', 'filter': '0-5'},
            {'label': '$5 - $10', 'filter': '5-10'},
            {'label': '$10 - $20', 'filter': '10-20'},
            {'label': '$20 - $40', 'filter': '20-40'},
            {'label': '$40+', 'filter': '40-10000'}
        ],
    },

    #-------------------------------------------------------------------------------
    'ru': {
        'value': "ru",
        'region_name': "Russia",
        'country_abbreviation': u'ru',
        'game_attributes': {
            'price': 'ru_price',
            'sale_price': 'ru_sale_price',
            'on_sale_since': 'ru_on_sale_since',
            'available_since': 'ru_available_since',
        },
        'numbers': RUB,
        'cost_filter_list': [
            {'label': u'< 150 pуб.', 'filter': u'0-150'},
            {'label': u'150 - 300 pуб.', 'filter': u'150-300'},
            {'label': u'300 - 500 pуб.', 'filter': u'300-500'},
            {'label': u'500 - 700 pуб.', 'filter': u'500-700'},
            {'label': u'700+ pуб.', 'filter': u'700-10000'}
        ],
    },

    #-------------------------------------------------------------------------------
    'br': {
        'value': "br",
        'region_name': "Brazil",
        'country_abbreviation': u'br',
        'game_attributes': {
            'price': 'br_price',
            'sale_price': 'br_sale_price',
            'on_sale_since': 'br_on_sale_since',
            'available_since': 'br_available_since',
        },
        'numbers': BRL,
        'cost_filter_list': [
            {'label': '< R$5', 'filter': '0-5'},
            {'label': 'R$5 - R$10', 'filter': '5-10'},
            {'label': 'R$10 - R$20', 'filter': '10-20'},
            {'label': 'R$20 - R$40', 'filter': '20-40'},
            {'label': 'R$40+', 'filter': '40-10000'}
        ],
    },
}
