"""Contains the spiders that will spider steam.
"""
from decimal import Decimal
import cherrypy
import datetime
import requests
import random
import robotparser
import time
from bs4 import BeautifulSoup
from lxml import etree

from jamaisvu import pglock

import hydro
import plumbing.auth

from gamestradamus import cli, timeutils, units
from gamestradamus.channel.steam import parse
from gamestradamus.taskqueue import tasks
from gamestradamus.regions import REGIONS

#-------------------------------------------------------------------------------
class ForbiddenRobotError(Exception):
    """Raised when a GET to a URL forbidden by robots.txt is requested."""

#-------------------------------------------------------------------------------
class BaseSpider(object):
    USER_AGENT = "Gauge Crawler (v.0.1)"

    #---------------------------------------------------------------------------
    def __init__(self, delay_between_requests):
        self.rp = robotparser.RobotFileParser()
        self.rp.set_url("http://steamcommunity.com/robots.txt")
        self.rp.read()

        self.urls = []
        self._last_request_time = None
        self.delay_between_requests = delay_between_requests
        self.session = requests.session()

    #---------------------------------------------------------------------------
    def wait(self):
        """Implements the rate limiting algorithm.

        Call this before you call your next GET and it will time.sleep for 
        self.delay_between_requests seconds.
        """
        # If we haven't made a request - we can definitely make a new one.
        if not self._last_request_time:
            return

        current = time.time()
        elapsed = current - self._last_request_time

        # Enough time has passed since the last message - we can continue.
        if elapsed >= self.delay_between_requests:
            return

        # Otherwise, sleep for the remaining time before a new message is
        # allowed.
        time.sleep(self.delay_between_requests - elapsed)

    #---------------------------------------------------------------------------
    def GET(self, url):
        """Perform a get on the URL, returning the requests response on success.

        Raises ForbiddenRobotError if this robot may not fetch that url.
        """
        self._last_request_time = time.time()
        if not self.rp.can_fetch(self.USER_AGENT, url):
            raise ForbiddenRobotError(
                    "%s is forbidden to %s" % (url, self.USER_AGENT))
        headers = {'user-agent': self.USER_AGENT}
        response = self.session.get(url, headers=headers, timeout=30.0)
        return response

    #---------------------------------------------------------------------------
    def POST(self, url, data):
        """Perform a POST on the URL, returning the requests response on success.

        Raises ForbiddenRobotError if this robot may not fetch that url.
        """
        self._last_request_time = time.time()
        if not self.rp.can_fetch(self.USER_AGENT, url):
            raise ForbiddenRobotError(
                    "%s is forbidden to %s" % (url, self.USER_AGENT))
        headers = {'user-agent': self.USER_AGENT}
        return self.session.post(url, headers=headers, data=data, timeout=30.0)

#-------------------------------------------------------------------------------
class GameSpider(BaseSpider):
    """Users steams search page to spider game prices.
    """

    #---------------------------------------------------------------------------
    def __init__(self, delay_between_requests, region='us'):
        super(GameSpider, self).__init__(delay_between_requests)
        self.region = region
        if self.region not in REGIONS:
            raise RuntimeError("%s is not one of our valid regions")

        self.pricing = REGIONS[self.region]
        self.country_abbreviation = self.pricing['country_abbreviation']
        self.price_attr = self.pricing['game_attributes']['price']
        self.sale_price_attr = self.pricing['game_attributes']['sale_price']
        self.available_since_attr = self.pricing['game_attributes']['available_since']
        self.store = cli.storage_manager()
        self.sandbox = self.store.new_sandbox()

    #---------------------------------------------------------------------------
    def process(self):
        games_url = 'http://store.steampowered.com/search/?sort_by=Name&sort_order=DESC&category1=998&page=%s&cc=%s'
        dlc_url = 'http://store.steampowered.com/search/?sort_by=Name&sort_order=DESC&category1=21&page=%s&cc=%s'

        categories_to_spider = (
                (games_url, False,),
                (dlc_url, True,),
            )
        parser = etree.HTMLParser()
        forty_eight_hours = datetime.timedelta(hours=48)
        # This is approximately 72 games per spider run.
        # It's calculated by assuming we want to update this column
        # for every game once a 48 hours period and that we are spidering
        # every 30 minutes. 
        allowed_available_since_updates_per_spider = 7000/96
        available_since_updated = 0

        # The genre updates are likely the heaviest portion of this
        # so limit it to 25 per spider. They *shouldn't* happen
        # often, so this should be plenty - but in case something
        # goes screwy - this will protect us from spamming our db
        allowed_genre_updates_per_spider = 25
        genres_updated = 0

        def get_game(app_id):
            """Use a view to get a game and return a named dict"""
            x = self.sandbox.store.view((
                units.Game,
                lambda g: (
                    g.id,
                    g.app_id,
                    g.genre,
                    getattr(g, self.price_attr),
                    getattr(g, self.sale_price_attr),
                    getattr(g, self.available_since_attr),
                    g.release,
                    g.name,
                ),
                lambda g: g.app_id == app_id
            ))
            if len(x) < 1:
                return None
            return {
                'id': x[0][0],
                'app_id': x[0][1],
                'genre': x[0][2],
                self.price_attr: x[0][3],
                self.sale_price_attr: x[0][4],
                self.available_since_attr: x[0][5],
                'release': x[0][6],
                'name': x[0][7],
            }

        for search_url, is_dlc in categories_to_spider:
            # Figure out which games are available for purchase.
            starting_page = 1
            ending_page = 2
            page = starting_page
            while page <= ending_page:
                print 'PAGE: [%s], DLC[%s], REGION[%s]' % (page, is_dlc, self.region)
                page_url = search_url % (page, self.country_abbreviation)
                response = self.GET(page_url)
                if response.status_code != 200:
                    continue
                doc = etree.fromstring(response.text, parser)
                ending_page = parse.get_last_search_page(doc)
                apps = parse.get_apps_from_search_results(doc, self.region)
                del doc

                game_ids_to_update = []

                for app in apps:
                    url = 'http://store.steampowered.com/app/%s' % app['app_id']

                    game = get_game(app['app_id'])
                    if game is None:
                        game = units.Game(**{
                            'app_id': app['app_id'],
                            'name': app['name'],
                            self.price_attr: '0.00',
                            self.sale_price_attr: '0.00',
                            'is_dlc': is_dlc
                        })
                        self.sandbox.memorize(game)
                        game = get_game(app['app_id'])

                    attributes_to_set = {}

                    # Store the fact that we have seen this in the store recently.
                    # The point of this field is to store when we last saw the game
                    # in the search page. It allows us to look for games that have disappeard
                    # from the search page and are possibly no longer for sale.
                    # but we don't have to update it every time we cron.
                    #
                    # Only update it once every 48 hours up to a maximum of about 72
                    # per spider run. The 72 per run of the spider will allow us to update
                    # every game over a 48 hour period - evenly distributing the number of updates
                    # issues per spider run. (lowering db load)
                    if available_since_updated < allowed_available_since_updates_per_spider:
                        now = timeutils.utcnow()
                        last_updated = game[self.available_since_attr]
                        if not last_updated or now - last_updated > forty_eight_hours:
                            available_since_updated += 1
                            attributes_to_set[self.available_since_attr] = timeutils.utcnow()

                    # Sometimes steam screws up and forgets the title of games, ignore when that happens.
                    if app['name'].lower() != 'uninitialized' and app['name'] != game['name']:
                        attributes_to_set['name'] = app['name']

                    # The release date of games are region specific and the steam search
                    # pages will show a different release date per request.
                    # see Transformers: War of Cybertron
                    #
                    # To avoid un-necessary updates, ONLY set the release date once for now.
                    if app['release'] is not None and game['release'] is None:
                        attributes_to_set['release'] = app['release']

                    # Steam will sometimes not include the genre on the steam search page.
                    # The old spider would happily remove the genre and then add it back in
                    # the next time it spiders. This is extra db load that we can void.
                    #
                    # Only update the genre if we're not remove the data.
                    if game['genre'] != app['genre'] and app['genre'] is not None:
                        # Rate limit the Genre updating to a small amount per spider.
                        if genres_updated < allowed_genre_updates_per_spider:
                            genres_updated += 1
                            units.normalize_genres(self.sandbox, game['id'], app['genre'])
                            attributes_to_set['genre'] = app['genre']

                    if app['price']:

                        # If the prices are different from what we have, then record
                        # this game id as one that needs to be spidered directly.
                        game_sale_price = game[self.sale_price_attr]
                        game_price = game[self.price_attr]
                        if app['sale_price'] != game_sale_price or app['price'] != game_price:
                            game_ids_to_update.append(game['id'])

                    # Update the game record if there are any new attributes to set.
                    if attributes_to_set:
                        attributes_to_set['id'] = game['id']
                        self.sandbox.store.schema['Game'].save(**attributes_to_set)

                self.sandbox.flush_all()

                # After we have committed our transaction, schedule a task to directly
                # spider and update the price for each game that we think has changed prices.
                for game_id in game_ids_to_update:
                    tasks.update_game_price.apply_async(**{
                        'args': (game_id, self.region),
                        'routing_key': 'tasks.plebs.parse-game-price',
                    })
                page += 1

#-------------------------------------------------------------------------------
class FriendSpider(BaseSpider):
    """Used to spider friends lists."""

    #---------------------------------------------------------------------------     
    def __init__(self, delay_between_requests):
        super(FriendSpider, self).__init__(delay_between_requests)
        self.store = cli.storage_manager()
        self.sandbox = self.store.new_sandbox()
        
    #---------------------------------------------------------------------------
    def process(self):
        rows, cols = self.store.db.fetch("""
            SELECT "UserAccount".openid
            FROM "UserAccountAttributes"
            INNER JOIN "UserAccount"
                ON "UserAccount".id = "UserAccountAttributes".user_account_id
            WHERE "UserAccount".openid <> 'Metascore'
            ORDER BY
                "UserAccountAttributes".games_last_updated IS NOT NULL DESC,
                "UserAccountAttributes".games_last_updated DESC
            LIMIT 10
        """) 
        for row in rows:
            url = '%s/friends' % row[0].replace('openid/id', 'profiles')
            try:
                response = self.GET(url)
                response.raise_for_status()
            except Exception, e:
                continue
 
            # Extract the links to the friends list.
            soup = BeautifulSoup(response.text)
            links = soup.findAll('a', **{'class': 'friendBlockLinkOverlay'})
            for link in links:
                openid = None

                if link['href'].startswith('http://steamcommunity.com/profiles/'):
                    openid = link['href'].replace('profiles', 'openid/id')

                elif link['href'].startswith('http://steamcommunity.com/id/'):
                    # We have to make a second request to get the openid number.
                    try:
                        response = self.GET(link['href'])    
                        response.raise_for_status()
                    except Exception, e:
                        continue

                    location = response.text.find('"steamid":"')
                    try:
                        steam_id_64 = int(response.text[(location + 11):(location + 28)])
                    except (TypeError, ValueError, IndexError), e:
                        continue
                    openid = 'http://steamcommunity.com/openid/id/%s' % (
                        response.text[(location + 11):(location + 28)],
                    )

                if openid:
                    user_account = self.sandbox.UserAccount(**{
                        'openid': openid
                    }) 
                    if user_account is None:
                        user_account = plumbing.auth.units.UserAccount(**{
                            'openid': openid
                        })
                        self.sandbox.memorize(user_account)

                        user_attrs = units.UserAccountAttributes(**{
                            'user_account_id': user_account.id
                        })
                        self.sandbox.memorize(user_attrs)

                        stats = units.UserAccountStatistics(**{
                            'user_account_id': user_account.id
                        })
                        self.sandbox.memorize(stats)

            self.sandbox.flush_all()




#-------------------------------------------------------------------------------
class AgeCheckSpider(BaseSpider):
    #---------------------------------------------------------------------------
    def check_response(self, url, response):
        age_check_url, snr = parse.get_agecheck_url(response.text)
        if age_check_url:
            d = random.choice(range(28))
            m = random.choice(range(12))
            y = random.choice(range(1968, 1981))
            payload = {'snr': snr, 'ageDay': d, 'ageMonth': m, 'ageYear': y}
            response = self.POST(age_check_url, data=payload)
            if response.status_code != 200:
                print "Error POSTING agecheck %s: %s\n%s\n" % (
                        url,
                        response.status_code,
                        response.text
                    )
                return None
            return self.GET(url)




#-------------------------------------------------------------------------------
if __name__ == "__main__":
    import sys
    import threading
    # 5 requests per second
    delay_between_requests = 1.0 / 5.0
    if len(sys.argv) < 2:
        sys.exit("Invalid argument")
    elif sys.argv[1] == "games":
        from gamestradamus.regions import REGIONS
        regions = sorted(REGIONS.keys())
        if len(sys.argv) >= 3:
            regions = sys.argv[2:]
        def run_1():
            for region in list(regions)[::4]:
                spider = GameSpider(delay_between_requests, region)
                spider.process()

        def run_2():
            for region in list(regions)[1::4]:
                spider = GameSpider(delay_between_requests, region)
                spider.process()

        def run_3():
            for region in list(regions)[2::4]:
                spider = GameSpider(delay_between_requests, region)
                spider.process()

        def run_4():
            for region in list(regions)[3::4]:
                spider = GameSpider(delay_between_requests, region)
                spider.process()

        threads = [
                threading.Thread(target=run_1),
                threading.Thread(target=run_2),
                threading.Thread(target=run_3),
                threading.Thread(target=run_4),
            ]

        for thread in threads:
            thread.start()
        for thread in threads:
            thread.join()
    elif sys.argv[1] == "friends":
        spider = FriendSpider(delay_between_requests)
        spider.process()

