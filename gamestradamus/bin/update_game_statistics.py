
import time
from gamestradamus import cli, units
from gamestradamus.taskqueue import tasks

if __name__ == "__main__":
    store = cli.storage_manager()
    sandbox = store.new_sandbox()

    games = sandbox.xrecall(
        units.Game,
        lambda x: x.is_dlc == False
    )
    stat_sets_processed = set()
    games = sorted(games, key=lambda x: x.app_id)
    for game in games:
        print 'Queueing App ID: %s' % game.app_id

        # Don't process games that are part of a set that we've already
        # processed.
        if game.stat_set_id and game.stat_set_id in stat_sets_processed:
            continue

        if game.stat_set_id:
            stat_sets_processed.add(game.stat_set_id)

        tasks.update_game_statistics.apply_async(**{
            'args': [game.id],
            'routing_key': 'tasks.background.update-game-statistics'
        })
        time.sleep(1)

    store.shutdown()

